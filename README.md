# Mongo and Pop
A quick 'mom and pop' webstore example via Node.js and MongoDB

## this repo uses 
* node.js backend 
* MongoDB / Mongoose database 
* Express / Bootstrap 4 front end 


## Installation 

- Clone the repo
- Install dependencies: `npm i`
- `cd` into `/mongoandpop`
- `npm start` to initialize server 
